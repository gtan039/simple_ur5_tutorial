
#include <ros/ros.h>
#include <ros/callback_queue.h>

#include "environment.h"

using namespace std;

int main(int argc, char** argv)
{
  ros::init(argc, argv, "grasp_execution");
  ros::AsyncSpinner spinner(1);
  spinner.start();
  ros::NodeHandle node_handle("~");

  std::cout << "Node start"<<std::endl;
  
  robot_model_loader::RobotModelLoaderPtr robot_model_loader(new robot_model_loader::RobotModelLoader("robot_description"));
  planning_scene_monitor::PlanningSceneMonitorPtr psm(new planning_scene_monitor::PlanningSceneMonitor(robot_model_loader));

  moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
  robot_model::RobotModelPtr robot_model = robot_model_loader->getModel();

  ros::Duration(2).sleep();

  robot_state::RobotStatePtr robot_state(new robot_state::RobotState(planning_scene_monitor::LockedPlanningSceneRO(psm)->getCurrentState()));
  
  //The PlanningPipeline object uses the ROS parameter server to determine the set of request adapters and the planning plugin to use
  planning_pipeline::PlanningPipelinePtr planning_pipeline(new planning_pipeline::PlanningPipeline(robot_model, node_handle, "planning_plugin", "request_adapters"));

  Environment environment(node_handle, &psm, &planning_scene_interface, &planning_pipeline, &robot_state);

  while (ros::ok())
  {
      ros::getGlobalCallbackQueue()->callAvailable(ros::WallDuration(0.1));
  }
  return 0;
}
