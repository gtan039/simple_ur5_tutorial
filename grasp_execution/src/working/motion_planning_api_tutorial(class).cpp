/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2012, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Sachin Chitta, Michael Lautman */

#include <pluginlib/class_loader.h>
#include <ros/ros.h>

// MoveIt
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/planning_interface/planning_interface.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/PlanningScene.h>
#include <moveit_visual_tools/moveit_visual_tools.h>

#include <boost/scoped_ptr.hpp>

namespace rvt = rviz_visual_tools;

class Robot
{
public:
  std::string PLANNING_GROUP;

  robot_model_loader::RobotModelLoader* robot_model_loader;
  robot_model::RobotModelPtr robot_model;
  //robot_state::RobotStatePtr* robot_state;
  robot_state::RobotStatePtr robot_state;
  const robot_state::JointModelGroup* joint_model_group;
  
  Robot(std::string planning_group, std::string robot_description)
  {
    PLANNING_GROUP = planning_group;
    robot_model_loader = new robot_model_loader::RobotModelLoader(robot_description);
    robot_model = robot_model_loader->getModel();
  
    robot_state = robot_state::RobotStatePtr (new robot_state::RobotState(robot_model));
    joint_model_group = robot_state->getJointModelGroup(PLANNING_GROUP);
  }

  ~Robot()
  {
    delete robot_model_loader;
    //delete robot_state;
  }

};

moveit_visual_tools::MoveItVisualTools InitVisualTools(std::string end_link, std::string pub_name)
{
  moveit_visual_tools::MoveItVisualTools visual_tools(end_link);
  visual_tools.loadRobotStatePub(pub_name);
  visual_tools.enableBatchPublishing();
  visual_tools.deleteAllMarkers();  // clear all old markers
  visual_tools.trigger();

  /* Remote control is an introspection tool that allows users to step through a high level script
     via buttons and keyboard shortcuts in RViz */
  visual_tools.loadRemoteControl();
  return visual_tools;
}

geometry_msgs::PoseStamped MakePose(std::string name,float x, float y, float z, float w)
{
  geometry_msgs::PoseStamped pose;
  pose.header.frame_id = name;
  pose.pose.position.x = x;
  pose.pose.position.y = y;
  pose.pose.position.z = z;
  pose.pose.orientation.w = w;
  return pose;
}

void DisplayGoalState(moveit_visual_tools::MoveItVisualTools visual_tools, planning_scene::PlanningScenePtr planning_scene,Eigen::Isometry3d text_pose)
{
  // Display the goal state
  visual_tools.publishRobotState(planning_scene->getCurrentStateNonConst(), rviz_visual_tools::GREEN);
  visual_tools.publishText(text_pose, "Joint Space Goal (2)", rvt::WHITE, rvt::XLARGE);
  visual_tools.trigger();
}

class Planner
{

public:   
  //planning_interface::MotionPlanRequest req;
  planning_interface::MotionPlanResponse res;
  //moveit_msgs::MotionPlanResponse response;
  planning_interface::PlanningContextPtr context;
  moveit_msgs::Constraints pose_goal;

  std::string planner_plugin_name;
  planning_interface::PlannerManagerPtr planner_instance;
  boost::scoped_ptr<pluginlib::ClassLoader<planning_interface::PlannerManager>> planner_plugin_loader;


  moveit_msgs::MotionPlanResponse *response_ = NULL;
  moveit_msgs::MotionPlanRequest *req_ = NULL;

  Planner(std::string planning_group)
  {
    response_ = new  moveit_msgs::MotionPlanResponse;
    req_ = new moveit_msgs::MotionPlanRequest;
    req_->group_name = planning_group;
  }

  ~Planner()
  {
    delete response_;
    delete req_;
  }

  void LoadPlugins(Robot *robot,ros::NodeHandle *node_handle)
  {
    if (!node_handle->getParam("planning_plugin", planner_plugin_name))
      ROS_FATAL_STREAM("Could not find planner plugin name");
    try
    {
      planner_plugin_loader.reset(new pluginlib::ClassLoader<planning_interface::PlannerManager>(
          "moveit_core", "planning_interface::PlannerManager"));
    }
    catch (pluginlib::PluginlibException& ex)
    {
      ROS_FATAL_STREAM("Exception while creating planning plugin loader " << ex.what());
    }
    try
    {
      planner_instance.reset(planner_plugin_loader->createUnmanagedInstance(planner_plugin_name));
      if (!planner_instance->initialize(robot->robot_model, node_handle->getNamespace()))
        ROS_FATAL_STREAM("Could not initialize planner instance");
      ROS_INFO_STREAM("Using planning interface '" << planner_instance->getDescription() << "'");
    }
    catch (pluginlib::PluginlibException& ex)
    {
      const std::vector<std::string>& classes = planner_plugin_loader->getDeclaredClasses();
      std::stringstream ss;
      for (std::size_t i = 0; i < classes.size(); ++i)
        ss << classes[i] << " ";
      ROS_ERROR_STREAM("Exception while loading planner '" << planner_plugin_name << "': " << ex.what() << std::endl << "Available plugins: " << ss.str());
    }
  }

  void SetPoseGoal(std::vector<double> tolerance_pose,std::vector<double> tolerance_angle,geometry_msgs::PoseStamped pose)
  {
    pose_goal = kinematic_constraints::constructGoalConstraints("panda_link8", pose, tolerance_pose, tolerance_angle);

    req_->goal_constraints.clear();
    req_->goal_constraints.push_back(pose_goal);
    //req_->group_name = planning_group;
    //AddGoal(pose_goal);
    //req.goal_constraints.push_back(pose_goal);
  }

  void SetJointGoal(Robot* robot, std::vector<double> joint_values)
  {
    robot_state::RobotState goal_state(robot->robot_model);
    moveit_msgs::Constraints joint_goal = kinematic_constraints::constructGoalConstraints(goal_state, robot->joint_model_group);
    //AddGoal(joint_goal);
    req_->goal_constraints.clear();
    req_->goal_constraints.push_back(joint_goal);
  }

  //void AddGoal(moveit_msgs::Constraints goal)
  //{
    //req_->goal_constraints.clear();
    //req_->goal_constraints.push_back(goal);
  //}

  void PublishTrajectory(ros::Publisher display_publisher,Robot *robot,moveit_msgs::DisplayTrajectory *display_trajectory,moveit_visual_tools::MoveItVisualTools *visual_tools)
  {
    
    display_trajectory->trajectory.push_back(response_->trajectory);
    visual_tools->publishTrajectoryLine(display_trajectory->trajectory.back(), robot->joint_model_group);
    visual_tools->trigger();
    display_publisher.publish(*display_trajectory);
    robot->robot_state->setJointGroupPositions(robot->joint_model_group, response_->trajectory.joint_trajectory.points.back().positions);
  }

  void DisplayGoalState(moveit_visual_tools::MoveItVisualTools visual_tools, planning_scene::PlanningScenePtr planning_scene,Eigen::Isometry3d text_pose)
  {
    // Display the goal state
    visual_tools.publishRobotState(planning_scene->getCurrentStateNonConst(), rviz_visual_tools::GREEN);
    visual_tools.publishText(text_pose, "Joint Space Goal (2)", rvt::WHITE, rvt::XLARGE);
    visual_tools.trigger();
  }

  void AddConstraints(geometry_msgs::QuaternionStamped quaternion, double min_corner[3], double max_corner[3])
  {
    req_->path_constraints = kinematic_constraints::constructGoalConstraints("panda_link8", quaternion);
    req_->workspace_parameters.min_corner.x = min_corner[0];
    req_->workspace_parameters.min_corner.y = min_corner[1];
    req_->workspace_parameters.min_corner.z = min_corner[2];

    req_->workspace_parameters.max_corner.x = max_corner[0];
    req_->workspace_parameters.max_corner.y = max_corner[1];
    req_->workspace_parameters.max_corner.z = max_corner[2];
  }

  void SolvePath(planning_scene::PlanningScenePtr planning_scene)
  {
    //planning_interface::MotionPlanResponse res;
    //moveit_msgs::MotionPlanResponse response;
    //planning_interface::PlanningContextPtr context
    context = planner_instance->getPlanningContext(planning_scene, *req_, res.error_code_);
    context->solve(res);
    std::cout << "Debug \n";
    res.getMessage(*response_);
  }
};

/*
planning_interface::PlannerManagerPtr LoadPlugins(Robot *robot,ros::NodeHandle *node_handle)
{
  std::string planner_plugin_name;
  planning_interface::PlannerManagerPtr planner_instance;
  boost::scoped_ptr<pluginlib::ClassLoader<planning_interface::PlannerManager>> planner_plugin_loader;
  if (!node_handle->getParam("planning_plugin", planner_plugin_name))
    ROS_FATAL_STREAM("Could not find planner plugin name");
  try
  {
    planner_plugin_loader.reset(new pluginlib::ClassLoader<planning_interface::PlannerManager>(
        "moveit_core", "planning_interface::PlannerManager"));
  }
  catch (pluginlib::PluginlibException& ex)
  {
    ROS_FATAL_STREAM("Exception while creating planning plugin loader " << ex.what());
  }
  try
  {
    planner_instance.reset(planner_plugin_loader->createUnmanagedInstance(planner_plugin_name));
    if (!planner_instance->initialize(robot->robot_model, node_handle->getNamespace()))
      ROS_FATAL_STREAM("Could not initialize planner instance");
    ROS_INFO_STREAM("Using planning interface '" << planner_instance->getDescription() << "'");
  }
  catch (pluginlib::PluginlibException& ex)
  {
    const std::vector<std::string>& classes = planner_plugin_loader->getDeclaredClasses();
    std::stringstream ss;
    for (std::size_t i = 0; i < classes.size(); ++i)
      ss << classes[i] << " ";
    ROS_ERROR_STREAM("Exception while loading planner '" << planner_plugin_name << "': " << ex.what() << std::endl << "Available plugins: " << ss.str());
  }
  return planner_instance;
}



planning_interface::MotionPlanRequest SetPoseGoal(std::string planning_group ,moveit_msgs::Constraints pose_goal)
{
  planning_interface::MotionPlanRequest req;
  req.group_name = planning_group;
  req.goal_constraints.push_back(pose_goal);
  return req;
}

void AddGoal(planning_interface::MotionPlanRequest *req, moveit_msgs::Constraints goal)
{
  req->goal_constraints.clear();
  req->goal_constraints.push_back(goal);
}

void PublishTrajectory(moveit_msgs::MotionPlanResponse* response,ros::Publisher display_publisher,Robot *robot,moveit_msgs::DisplayTrajectory *display_trajectory,moveit_visual_tools::MoveItVisualTools *visual_tools)
{
 
  display_trajectory->trajectory.push_back(response->trajectory);
  visual_tools->publishTrajectoryLine(display_trajectory->trajectory.back(), robot->joint_model_group);
  visual_tools->trigger();
  display_publisher.publish(*display_trajectory);
  robot->robot_state->setJointGroupPositions(robot->joint_model_group, response->trajectory.joint_trajectory.points.back().positions);
}



void AddConstraints(planning_interface::MotionPlanRequest* req, geometry_msgs::QuaternionStamped quaternion, double min_corner[3], double max_corner[3])
{
  req->path_constraints = kinematic_constraints::constructGoalConstraints("panda_link8", quaternion);
  req->workspace_parameters.min_corner.x = min_corner[0];
  req->workspace_parameters.min_corner.y = min_corner[1];
  req->workspace_parameters.min_corner.z = min_corner[2];

  req->workspace_parameters.max_corner.x = max_corner[0];
  req->workspace_parameters.max_corner.y = max_corner[1];
  req->workspace_parameters.max_corner.z = max_corner[2];
}

moveit_msgs::MotionPlanResponse SolvePath(planning_interface::PlannerManagerPtr planner_instance,planning_scene::PlanningScenePtr planning_scene,planning_interface::MotionPlanRequest* req)
{
  planning_interface::MotionPlanResponse res;
  moveit_msgs::MotionPlanResponse response;
  planning_interface::PlanningContextPtr context = planner_instance->getPlanningContext(planning_scene, *req, res.error_code_);
  context->solve(res);
  std::cout << "Debug \n";
  res.getMessage(response);
  return response;
}*/

int main(int argc, char** argv)
{
  //Declare names
  const std::string node_name = "motion_planning_tutorial";
  std::string robot_name = "panda_arm";


  ros::init(argc, argv, node_name);
  ros::AsyncSpinner spinner(1);
  spinner.start();
  ros::NodeHandle node_handle("~");

  // Visualization
  // ^^^^^^^^^^^^^
  // The package MoveItVisualTools provides many capabilties for visualizing objects, robots,
  // and trajectories in RViz as well as debugging tools such as step-by-step introspection of a script

  moveit_visual_tools::MoveItVisualTools visual_tools = InitVisualTools("panda_link0", "/display_robot_state");
  
  /* RViz provides many types of markers, in this demo we will use text, cylinders, and spheres*/
  Eigen::Isometry3d text_pose = Eigen::Isometry3d::Identity();
  text_pose.translation().z() = 1.75;
  visual_tools.publishText(text_pose, "Motion Planning API Demo", rvt::WHITE, rvt::XLARGE);
  /* Batch publishing is used to reduce the number of messages being sent to RViz for large visualizations */
  visual_tools.trigger();
  /* We can also use visual_tools to wait for user input */
  visual_tools.prompt("Press 'next' in the RvizVisualToolsGui window to start the demo");


// A tolerance of 0.01 m is specified in position
  // and 0.01 radians in orientation
  std::vector<double> tolerance_pose(3, 0.01);
  std::vector<double> tolerance_angle(3, 0.01);
  ros::Publisher display_publisher = node_handle.advertise<moveit_msgs::DisplayTrajectory>("/move_group/display_planned_path", 1, true);
  //moveit_msgs::MotionPlanResponse response;

  Robot robot(robot_name,"robot_description");

  Planner planner(robot.PLANNING_GROUP);

  planning_scene::PlanningScenePtr planning_scene(new planning_scene::PlanningScene(robot.robot_model));// Construct a :planning_scene:`PlanningScene`
  planning_scene->getCurrentStateNonConst().setToDefaultValues(robot.joint_model_group, "ready"); // Configure a valid robot state
  
  planner.LoadPlugins(&robot, &node_handle);
  //planning_interface::PlannerManagerPtr planner_instance = LoadPlugins(&robot, &node_handle);


  // Pose Goal
  // ^^^^^^^^^

  visual_tools.publishRobotState(planning_scene->getCurrentStateNonConst(), rviz_visual_tools::GREEN);
  visual_tools.trigger();
  
  geometry_msgs::PoseStamped pose = MakePose("panda_link0", 0.3,0.4,0.75,1.0);

  planner.SetPoseGoal(tolerance_pose, tolerance_angle, pose);
  planner.SolvePath(planning_scene);
  //moveit_msgs::Constraints pose_goal = kinematic_constraints::constructGoalConstraints("panda_link8", pose, tolerance_pose, tolerance_angle);
  //AddGoal(&req,pose_goal);
  //planning_interface::MotionPlanRequest req =  SetPoseGoal(robot.PLANNING_GROUP ,pose_goal);
  //response = SolvePath(planner_instance, planning_scene, &req);


  // Visualize the result
  // ^^^^^^^^^^^^^^^^^^^^
  //res.getMessage(response);
  moveit_msgs::DisplayTrajectory display_trajectory;
  display_trajectory.trajectory_start = planner.response_->trajectory_start;

  planner.PublishTrajectory(display_publisher,&robot, &display_trajectory,&visual_tools);
  planning_scene->setCurrentState(*(robot.robot_state).get());
  DisplayGoalState(visual_tools, planning_scene,text_pose);

  visual_tools.prompt("Press 'next' in the RvizVisualToolsGui window to continue the demo");

  // Joint Space Goals
  // ^^^^^^^^^^^^^^^^^
  // Now, setup a joint space goal

  std::cout<<"JOINT PLANNING";
  robot_state::RobotState goal_state(robot.robot_model);
  std::vector<double> joint_values = { -1.0, 0.7, 0.7, -1.5, -0.7, 2.0, 0.0 };
  
  planner.SetJointGoal(&robot, joint_values);

  //goal_state.setJointGroupPositions(robot.joint_model_group, joint_values);
  //moveit_msgs::Constraints joint_goal = kinematic_constraints::constructGoalConstraints(goal_state, robot.joint_model_group);
  //AddGoal(&req,joint_goal);

  /* Re-construct the planning context */
  planner.SolvePath(planning_scene);


  planner.PublishTrajectory(display_publisher,&robot, &display_trajectory,&visual_tools);
  planning_scene->setCurrentState(*(robot.robot_state).get());
  DisplayGoalState(visual_tools, planning_scene,text_pose);

  /* Wait for user input */
  visual_tools.prompt("Press 'next' in the RvizVisualToolsGui window to continue the demo");

  /* Now, we go back to the first goal to prepare for orientation constrained planning */
  planner.SetPoseGoal(tolerance_pose, tolerance_angle, pose);

  planner.SolvePath(planning_scene);

  planner.PublishTrajectory(display_publisher,&robot, &display_trajectory,&visual_tools);
  planning_scene->setCurrentState(*(robot.robot_state).get());
  DisplayGoalState(visual_tools, planning_scene,text_pose);

  /* Wait for user input */
  visual_tools.prompt("Press 'next' in the RvizVisualToolsGui window to continue the demo");
  

  // Adding Path Constraints
  // ^^^^^^^^^^^^^^^^^^^^^^^
  std::cout<<"SAME AS FIRST";

  pose = MakePose("panda_link0", 0.32,-0.25,0.65,1.0);

  planner.SetPoseGoal(tolerance_pose, tolerance_angle, pose);
  //moveit_msgs::Constraints pose_goal_2 = kinematic_constraints::constructGoalConstraints("panda_link8", pose, tolerance_pose, tolerance_angle);
  //AddGoal(&req,pose_goal_2);
  
  geometry_msgs::QuaternionStamped quaternion;
  quaternion.header.frame_id = "panda_link0";
  quaternion.quaternion.w = 1.0;

  double min_corner[3] = {-2.0,-2.0,-2.0}; // Min 3d point of a planning volume
  double max_corner[3] = {2.0,2.0,2.0};

  //AddConstraints(&req, quaternion, min_corner, max_corner);
  planner.AddConstraints(quaternion, min_corner, max_corner);
 

  planner.SolvePath(planning_scene);

  planner.PublishTrajectory(display_publisher,&robot, &display_trajectory,&visual_tools);
  planning_scene->setCurrentState(*(robot.robot_state).get());
  DisplayGoalState(visual_tools, planning_scene,text_pose);

  visual_tools.prompt("Press 'next' in the RvizVisualToolsGui window to exit the demo");

  planner.planner_instance.reset();

  return 0;
}
