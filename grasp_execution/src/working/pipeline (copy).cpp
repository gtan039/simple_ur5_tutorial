/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, SRI International
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of SRI International nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Sachin Chitta, Dave Coleman, Mike Lautman */


#include <pluginlib/class_loader.h>
#include <ros/ros.h>

// MoveIt
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_state/conversions.h>
#include <moveit/planning_pipeline/planning_pipeline.h>
#include <moveit/planning_interface/planning_interface.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/PlanningScene.h>

#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>

#include <moveit_visual_tools/moveit_visual_tools.h>
#include <tf2/LinearMath/Quaternion.h>


int MoveTo(geometry_msgs::PoseStamped pose,std::string group_name, planning_pipeline::PlanningPipelinePtr planning_pipeline,planning_scene_monitor::PlanningSceneMonitorPtr psm,   moveit_msgs::MotionPlanResponse* response)
{
  std::vector<double> tolerance_pose(3, 0.01);
  std::vector<double> tolerance_angle(3, 0.01);
  moveit_msgs::Constraints pose_goal = kinematic_constraints::constructGoalConstraints("ee_link", pose, tolerance_pose, tolerance_angle);

  planning_interface::MotionPlanRequest req;
  req.group_name = group_name;
  req.goal_constraints.push_back(pose_goal);

  planning_interface::MotionPlanResponse res;  

  {planning_scene_monitor::LockedPlanningSceneRO lscene(psm);
  planning_pipeline->generatePlan(lscene, req, res);}

  /* Check that the planning was successful */
  if (res.error_code_.val != res.error_code_.SUCCESS)
  {ROS_ERROR("Could not compute plan successfully");
    return 0;}

  /* Visualize the trajectory */
  res.getMessage(*response);
  moveit::planning_interface::MoveGroupInterface::Plan my_plan;
  moveit::planning_interface::MoveGroupInterface move_group(group_name);
  my_plan.trajectory_ = response->trajectory;
  move_group.execute(my_plan);

  return 1;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "pipeline");
  ros::AsyncSpinner spinner(1);
  spinner.start();
  ros::NodeHandle node_handle("~");



  robot_model_loader::RobotModelLoaderPtr robot_model_loader(new robot_model_loader::RobotModelLoader("robot_description"));
  planning_scene_monitor::PlanningSceneMonitorPtr psm(new planning_scene_monitor::PlanningSceneMonitor(robot_model_loader));

  moveit::planning_interface::PlanningSceneInterface planning_scene_interface;


  /* listen for planning scene messages on topic _______ and apply them to the internal planning scene accordingly */
  psm->startSceneMonitor();
  /* listens to changes of world geometry, collision objects, and (optionally) octomaps*/
  psm->startWorldGeometryMonitor();
  /* listen to joint state updates as well as changes in attached collision objects and update the internal planning scene accordingly*/
  psm->startStateMonitor();

  /* We can also use the RobotModelLoader to get a robot model which contains the robot's kinematic information */
  robot_model::RobotModelPtr robot_model = robot_model_loader->getModel();
  // ROS_INFO("Model frame: %s", kinematic_model->getModelFrame().c_str());
  
  
  /* We can get the most up to date robot state from the PlanningSceneMonitor by locking the internal planning scene
    for reading. This lock ensures that the underlying scene isn't updated while we are reading it's state.
    RobotState's are useful for computing the forward and inverse kinematics of the robot among many other uses */

  ros::Duration(2).sleep();

  robot_state::RobotStatePtr robot_state(new robot_state::RobotState(planning_scene_monitor::LockedPlanningSceneRO(psm)->getCurrentState()));

  /* Create a JointModelGroup to keep track of the current robot pose and planning group.*/
  const robot_model::JointModelGroup* joint_model_group_arm = robot_state->getJointModelGroup("manipulator");
  const robot_model::JointModelGroup* joint_model_group_hand = robot_state->getJointModelGroup("hand");

  //The PlanningPipeline object uses the ROS parameter server to determine the set of request adapters and the planning plugin to use
  planning_pipeline::PlanningPipelinePtr planning_pipeline(new planning_pipeline::PlanningPipeline(robot_model, node_handle, "planning_plugin", "request_adapters"));

  tf2::Quaternion myQuaternion;
  myQuaternion.setRPY( 1.5707963, 0.5235988*3, -1.5707963); 

  geometry_msgs::PoseStamped pose;
  pose.header.frame_id = "base_link";
    
  pose.pose.orientation.x = myQuaternion[0];
  pose.pose.orientation.y = myQuaternion[1];
  pose.pose.orientation.z = myQuaternion[2];
  pose.pose.orientation.w = myQuaternion[3];

  pose.pose.position.x = -0.2;
  pose.pose.position.y = 0.1;
  pose.pose.position.z = 0.2;
  moveit_msgs::MotionPlanResponse response;


  if(MoveTo(pose, "manipulator", planning_pipeline,psm, &response) ==0){return 0;}


  // //Set pose and angle tolerance.
  // std::vector<double> tolerance_pose(3, 0.01);
  // std::vector<double> tolerance_angle(3, 0.01);
  // moveit_msgs::Constraints pose_goal = kinematic_constraints::constructGoalConstraints("ee_link", pose, tolerance_pose, tolerance_angle);

  // planning_interface::MotionPlanRequest req;
  // req.group_name = "manipulator";
  // req.goal_constraints.push_back(pose_goal);

  // planning_interface::MotionPlanResponse res;  

  // {planning_scene_monitor::LockedPlanningSceneRO lscene(psm);
  // planning_pipeline->generatePlan(lscene, req, res);}

  // /* Check that the planning was successful */
  // if (res.error_code_.val != res.error_code_.SUCCESS)
  // {ROS_ERROR("Could not compute plan successfully");
  //   return 0;}

  // /* Visualize the trajectory */
  // res.getMessage(response);
  // moveit::planning_interface::MoveGroupInterface::Plan my_plan;
  moveit::planning_interface::MoveGroupInterface move_group("manipulator");
  // my_plan.trajectory_ = response.trajectory;
  // move_group.execute(my_plan);

  ros::Duration(2).sleep();

  //---------------------------------------------------------------Create collision object -----------------------------------------------//

  // Define a collision object ROS message.
  moveit_msgs::CollisionObject collision_object;
  collision_object.header.frame_id = move_group.getPlanningFrame();
  // The id of the object is used to identify it.
  collision_object.id = "box1";
  // Define a box to add to the world.
  shape_msgs::SolidPrimitive primitive;
  primitive.type = primitive.BOX;
  primitive.dimensions.resize(3);
  primitive.dimensions[0] = 0.1;
  primitive.dimensions[1] = 0.05;
  primitive.dimensions[2] = 0.1;
  // Define a pose for the box (specified relative to frame_id)
  geometry_msgs::Pose box_pose;
  box_pose.orientation.w = 1.0;
  box_pose.position.x = -0.4;
  box_pose.position.y = 0.3;
  box_pose.position.z = 0.05;
  collision_object.primitives.push_back(primitive);
  collision_object.primitive_poses.push_back(box_pose);
  collision_object.operation = collision_object.ADD;
  std::vector<moveit_msgs::CollisionObject> collision_objects;
  collision_objects.push_back(collision_object);
  planning_scene_interface.applyCollisionObjects(collision_objects);
  ros::Duration(2).sleep();

  //---------------------------------------------------------------Grasp Box -----------------------------------------------//

  robot_state = planning_scene_monitor::LockedPlanningSceneRO(psm)->getCurrentStateUpdated(response.trajectory_start);
  pose.pose = move_group.getCurrentPose().pose;
  myQuaternion.setRPY(1.5707963,1.5707963,-1.5707963); 
  pose.pose.orientation.x = myQuaternion[0];
  pose.pose.orientation.y = myQuaternion[1];
  pose.pose.orientation.z = myQuaternion[2];
  pose.pose.orientation.w = myQuaternion[3];
  
  pose.pose.position.x = -0.4;
  pose.pose.position.y = 0.3;
  pose.pose.position.z = 0.225;

  if(MoveTo(pose, "manipulator", planning_pipeline,psm, &response) ==0){return 0;}


  // pose_goal = kinematic_constraints::constructGoalConstraints("ee_link", pose, tolerance_pose, tolerance_angle);
  // req.goal_constraints.clear();
  // req.goal_constraints.push_back(pose_goal);

  // {
  // // Before planning, Read Only lock on the planning scene so that it does not modify the world representation while planning
  // planning_scene_monitor::LockedPlanningSceneRO lscene(psm);
  //   /* Now, call the pipeline and check whether planning was successful. */
  // planning_pipeline->generatePlan(lscene, req, res);
  // }
  // /* Check that the planning was successful */
  // if (res.error_code_.val != res.error_code_.SUCCESS){
  //   ROS_ERROR("Could not compute plan successfully");
  //   return 0;}

  // res.getMessage(response);
  // my_plan.trajectory_ = response.trajectory;
  // move_group.execute(my_plan);

  //--------------------------------------------------------------- Actuates  -----------------------------------------------//


  moveit::planning_interface::MoveGroupInterface move_group_hand("hand");
  moveit::core::RobotStatePtr current_state = move_group_hand.getCurrentState();
  std::vector<double> joint_group_positions;
  current_state->copyJointGroupPositions(joint_model_group_hand, joint_group_positions);
  moveit::planning_interface::MoveGroupInterface::Plan my_plan;
  float pos = 0.1;
  joint_group_positions[2] = pos;  // radians. 0.8 max clamp, 0.3
  joint_group_positions[6] = pos;  // radians. 0.8 min clamp

  move_group_hand.setJointValueTarget(joint_group_positions);
  bool success = (move_group_hand.plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);

  while(success)
  {
    pos += 0.01;
    joint_group_positions[2] = pos;  // radians. 0.8 max clamp, 0.3
    joint_group_positions[6] = pos;  // radians. 0.8 min clamp
    move_group_hand.setJointValueTarget(joint_group_positions);
    success = (move_group_hand.plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
  }
  pos -= 0.01;
  joint_group_positions[2] = pos;  // radians. 0.8 max clamp, 0.3
  joint_group_positions[6] = pos;  // radians. 0.8 min clamp
  move_group_hand.setJointValueTarget(joint_group_positions);

  move_group_hand.plan(my_plan);
  move_group_hand.execute(my_plan);

  //-----------------------------------------------------------Attach object------------------------------------------//
  move_group_hand.attachObject(collision_object.id);

  pose.pose.position.x = -0.2;
  pose.pose.position.y = 0.1;
  pose.pose.position.z = 0.3;

  if(MoveTo(pose, "manipulator", planning_pipeline,psm, &response) ==0){return 0;}

  // pose_goal = kinematic_constraints::constructGoalConstraints("ee_link", pose, tolerance_pose, tolerance_angle);
  // req.goal_constraints.clear();
  // req.goal_constraints.push_back(pose_goal);

  // {
  // // Before planning, Read Only lock on the planning scene so that it does not modify the world representation while planning
  // planning_scene_monitor::LockedPlanningSceneRO lscene(psm);
  //   /* Now, call the pipeline and check whether planning was successful. */
  // planning_pipeline->generatePlan(lscene, req, res);
  // }
  // /* Check that the planning was successful */
  // if (res.error_code_.val != res.error_code_.SUCCESS){
  //   ROS_ERROR("Could not compute plan successfully");
  //   return 0;}

  // res.getMessage(response);
  // my_plan.trajectory_ = response.trajectory;
  // move_group.execute(my_plan);

  pose.pose.position.z = 0.225;
  if(MoveTo(pose, "manipulator", planning_pipeline,psm, &response) ==0){return 0;}


  // pose_goal = kinematic_constraints::constructGoalConstraints("ee_link", pose, tolerance_pose, tolerance_angle);
  // req.goal_constraints.clear();
  // req.goal_constraints.push_back(pose_goal);

  // {
  // // Before planning, Read Only lock on the planning scene so that it does not modify the world representation while planning
  // planning_scene_monitor::LockedPlanningSceneRO lscene(psm);
  //   /* Now, call the pipeline and check whether planning was successful. */
  // planning_pipeline->generatePlan(lscene, req, res);
  // }
  // /* Check that the planning was successful */
  // if (res.error_code_.val != res.error_code_.SUCCESS){
  //   ROS_ERROR("Could not compute plan successfully");
  //   return 0;}

  // res.getMessage(response);
  // my_plan.trajectory_ = response.trajectory;
  // move_group.execute(my_plan);

  
  move_group_hand.detachObject(collision_object.id);

  pose.pose.position.z = 0.35;
  if(MoveTo(pose, "manipulator", planning_pipeline,psm, &response) ==0){return 0;}

  // pose_goal = kinematic_constraints::constructGoalConstraints("ee_link", pose, tolerance_pose, tolerance_angle);
  // req.goal_constraints.clear();
  // req.goal_constraints.push_back(pose_goal);

  // {
  // // Before planning, Read Only lock on the planning scene so that it does not modify the world representation while planning
  // planning_scene_monitor::LockedPlanningSceneRO lscene(psm);
  //   /* Now, call the pipeline and check whether planning was successful. */
  // planning_pipeline->generatePlan(lscene, req, res);
  // }
  // /* Check that the planning was successful */
  // if (res.error_code_.val != res.error_code_.SUCCESS){
  //   ROS_ERROR("Could not compute plan successfully");
  //   return 0;}

  // res.getMessage(response);
  // my_plan.trajectory_ = response.trajectory;
  // move_group.execute(my_plan);

  ros::shutdown();
  return 0;
}

