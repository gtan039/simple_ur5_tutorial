#ifndef ENVIRONMENT_H_
#define ENVIRONEMNT_H_
/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, SRI International
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of SRI International nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Sachin Chitta, Dave Coleman, Mike Lautman */


#include <pluginlib/class_loader.h>
#include <ros/ros.h>

// MoveIt
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_state/conversions.h>
#include <moveit/planning_pipeline/planning_pipeline.h>
#include <moveit/planning_interface/planning_interface.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/PlanningScene.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit_visual_tools/moveit_visual_tools.h>


#include <geometry_msgs/TransformStamped.h>
#include <string>
#include <grasp_execution/GraspPose.h>
#include <cv_bridge/cv_bridge.h>

#include "ur_msgs/SetIO.h"
#include <math.h>
class Environment
{


public:
  ros::Subscriber grasp_planner_sub;

  std::vector<moveit_msgs::CollisionObject> grasp_objects;
  // std::vector<geometry_msgs::TransformStamped> grasp_objects_tf;
  // static tf2_ros::TransformBroadcaster grasp_objects_tf_br;

  /* Create a JointModelGroup to keep track of the current robot pose and planning group.*/
  const robot_model::JointModelGroup* joint_model_group_arm ;
  const robot_model::JointModelGroup* joint_model_group_hand;

  moveit::planning_interface::MoveGroupInterface* move_group_arm;
  moveit::planning_interface::MoveGroupInterface* move_group_hand;
  moveit::planning_interface::MoveGroupInterface* move_group_camera;

  moveit::planning_interface::PlanningSceneInterface planning_scene_interface;

  moveit_msgs::MotionPlanResponse response;
  planning_scene_monitor::PlanningSceneMonitorPtr psm;
  planning_pipeline::PlanningPipelinePtr planning_pipeline;
  robot_state::RobotStatePtr robot_state;

  std::vector<double> home_pose= {M_PI/2,-M_PI/2,M_PI/2,0,M_PI/2,-M_PI/2};
  geometry_msgs::TransformStamped transformStamped_robot_camera;

  float table_depth;
  float conveyor_height;

  std::string camera_frame;
  std::string robot_frame; 
  std::string robot_group;
  std::string ee_group;
  std::string pose_publisher;
  
  std::vector<std::string> object_ids;
  ros::ServiceClient client;
  bool sim;

  

  Environment(ros::NodeHandle nh,  planning_scene_monitor::PlanningSceneMonitorPtr* psm_,  moveit::planning_interface::PlanningSceneInterface* planning_scene_interface_,planning_pipeline::PlanningPipelinePtr* planning_pipeline_, robot_state::RobotStatePtr* robot_state_)
  {
    client = nh.serviceClient<ur_msgs::SetIO>("/ur_driver/set_io");
    camera_frame = "camera_frame";
    robot_frame = "base_link";
    robot_group = "manipulator";
    ee_group = "gripper";
    pose_publisher = "/grasp_poses";
    planning_scene_interface = *planning_scene_interface_;
    psm = *psm_;
    planning_pipeline = *planning_pipeline_;
    robot_state = *robot_state_;
    table_depth = 0.6;
    conveyor_height = 0.0555;

    move_group_arm = new moveit::planning_interface::MoveGroupInterface(robot_group);
    move_group_arm->setPlanningTime(10.0);
    move_group_hand = new moveit::planning_interface::MoveGroupInterface(ee_group);
    

    joint_model_group_arm = robot_state->getJointModelGroup(robot_group);
    //joint_model_group_hand = robot_state->getJointModelGroup(ee_group);

    psm->startSceneMonitor(); /* listen for planning scene messages on topic _______ and apply them to the internal planning scene accordingly */
    psm->startWorldGeometryMonitor();  /* listens to changes of world geometry, collision objects, and (optionally) octomaps*/
    psm->startStateMonitor(); /* listen to joint state updates as well as changes in attached collision objects and update the internal planning scene accordingly*/
    
    ros::Duration(1).sleep();
    grasp_planner_sub = nh.subscribe(pose_publisher, 1, &Environment::GraspObjects,this);

    // tf2_ros::TransformBroadcaster tfb_robot_camera;
    transformStamped_robot_camera.header.frame_id = robot_frame;
    transformStamped_robot_camera.child_frame_id = camera_frame;
    transformStamped_robot_camera.transform.translation.x = -0.5;  //Get values from urdf
    transformStamped_robot_camera.transform.translation.y = 0.15; //Get values from urdf
    transformStamped_robot_camera.transform.translation.z = 0.6; //Get values from urdf
    tf2::Quaternion q;
    q.setRPY(3.14159, 0, 0); //Get values from urdf
    transformStamped_robot_camera.transform.rotation.x = q.x();
    transformStamped_robot_camera.transform.rotation.y = q.y();
    transformStamped_robot_camera.transform.rotation.z = q.z();
    transformStamped_robot_camera.transform.rotation.w = q.w();

    nh.getParam("/grasp_execution/sim", sim);      

    if(sim)
    {
      std::cout << "Simulated Robot" << std::endl;
      
    }
    else
    {
      std::cout << "Real Robot" << std::endl;
    }

    if(MoveToJointValues(home_pose, move_group_arm) == 0){return;} // Move to home position


  }

  int MoveTo(geometry_msgs::PoseStamped pose,moveit::planning_interface::MoveGroupInterface* move_group)
  {
    move_group->setStartStateToCurrentState();
    move_group->setPlanningTime(5.0f);   
    //move_group->setPlannerId("RRTkConfigDefault"); 
    //move_group->setPlannerId("KPIECEkConfigDefault"); 

    move_group->setPoseTarget(pose, "ee_link");

    moveit::planning_interface::MoveGroupInterface::Plan plan;
    bool success = move_group->plan(plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS;
    if(success)
    {
      ROS_INFO_STREAM("Executing Robot Motion");
      success = move_group->execute(plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS;
    }
    else
    {
      return 0;
    }
  }

  int MoveToJointValues(std::vector<double> joint_values,moveit::planning_interface::MoveGroupInterface* move_group)
  {
    move_group->setJointValueTarget(joint_values);
    moveit::planning_interface::MoveGroupInterface::Plan plan;
    bool success = move_group->plan(plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS;
    if(success)
    {
      ROS_INFO_STREAM("Executing Joint Robot Motion");
      success = move_group_arm->execute(plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS;
    }
    else
    {
      return 0;
    }
  }


  int MoveTo(geometry_msgs::PoseStamped pose,moveit::planning_interface::MoveGroupInterface* move_group, planning_pipeline::PlanningPipelinePtr planning_pipeline,planning_scene_monitor::PlanningSceneMonitorPtr psm,   moveit_msgs::MotionPlanResponse* response, std::string group_name)
  {

    std::vector<double> tolerance_pose(3, 0.01);
    std::vector<double> tolerance_angle(3, 0.01);

    planning_interface::MotionPlanRequest req;
    moveit_msgs::Constraints pose_goal = kinematic_constraints::constructGoalConstraints("ee_link", pose, tolerance_pose, tolerance_angle);
    req.group_name = group_name;
    req.goal_constraints.push_back(pose_goal);
    
    std::cout<< "Inside Moveto" << std::endl;
    planning_interface::MotionPlanResponse res;  


    {planning_scene_monitor::LockedPlanningSceneRO lscene(psm);
    planning_pipeline->generatePlan(lscene, req, res);}
    /* Check that the planning was successful */
    if (res.error_code_.val != res.error_code_.SUCCESS)
    {ROS_ERROR("Could not compute plan successfully");
      return 0;}

    /* Visualize the trajectory */
    res.getMessage(*response);
    // moveit::planning_interface::MoveGroupInterface move_group("manipulator");
    moveit::planning_interface::MoveGroupInterface::Plan my_plan;
    my_plan.trajectory_ = response->trajectory;
    move_group->execute(my_plan);
    return 1;
  }

  //Gripping function for 2 Fingered gripper. Currently restricted to the Robotiq 2F-85 Gripper.
  void TwoFingerGrasp()
  {
    moveit::core::RobotStatePtr current_state = move_group_hand->getCurrentState();
    std::vector<double> joint_group_positions;
    current_state->copyJointGroupPositions(joint_model_group_hand, joint_group_positions);
    moveit::planning_interface::MoveGroupInterface::Plan my_plan;

    //Get an initial starting point for joint positions 
    float pos = 0.1;

    //KIV: 
    //Sets the joint position for gripper_finger1_joint and gripper_finger2_joint, which is currently restricted to the robotiq 2f-85
    //Maybe check if other grippers are also designed the same way (for the urdf)
    joint_group_positions[2] = pos;  // radians. 0.8 max clamp, 0.3
    //joint_group_positions[6] = pos;  // radians. 0.8 min clamp

    move_group_hand->setJointValueTarget(joint_group_positions);
    bool success = (move_group_hand->plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);// Check if initial path planning for gripper works

    //Continue planning again until collision happens. This allows for the the gripper to just grip the object rather than squashing it
    while(success)
    {
      pos += 0.01;
      joint_group_positions[2] = pos;  // radians. 0.8 max clamp, 0.3
      //joint_group_positions[6] = pos;  // radians. 0.8 min clamp
      move_group_hand->setJointValueTarget(joint_group_positions);
      success = (move_group_hand->plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
    }

    //Current plan collides, so go back one iteration (Which should be a success)
    pos -= 0.01;
    joint_group_positions[2] = pos;  // radians. 0.8 max clamp, 0.3
    //joint_group_positions[6] = pos;  // radians. 0.8 min clamp
    move_group_hand->setJointValueTarget(joint_group_positions);

    move_group_hand->plan(my_plan);
    move_group_hand->execute(my_plan);
  }

  void TwoFingerRelease()
  {
    moveit::core::RobotStatePtr current_state = move_group_hand->getCurrentState();
    std::vector<double> joint_group_positions;
    current_state->copyJointGroupPositions(joint_model_group_hand, joint_group_positions);
    moveit::planning_interface::MoveGroupInterface::Plan my_plan;

    joint_group_positions[2] = 0;  // radians. 0.8 max clamp, 0.3
    //joint_group_positions[6] = 0;  // radians. 0.8 min clamp
    move_group_hand->setJointValueTarget(joint_group_positions);

    move_group_hand->plan(my_plan);
    move_group_hand->execute(my_plan);
  }

  //Function that converts pixel to real world coordinates. 
  float pixel_to_m(int pixel)
  {
    float conversion = 1;
    return pixel*conversion;
  }
  
  void PrintPose(geometry_msgs::Pose pose)
  {
    std::cout << "---------------------------------------------" << std::endl;
    std::cout << "x: "<<pose.position.x<<std::endl;
    std::cout << "y: "<<pose.position.y<<std::endl;
    std::cout << "z: "<<pose.position.z<<std::endl;
    std::cout << "x: "<<pose.orientation.x<<std::endl;
    std::cout << "y: "<<pose.orientation.y<<std::endl;
    std::cout << "z: "<<pose.orientation.z<<std::endl;
    std::cout << "w: "<<pose.orientation.w<<std::endl;
    std::cout << "---------------------------------------------" << std::endl;
  }

  void SuctionOn()
  {
    ur_msgs::SetIO srv;
    //Turn on suction
    srv.request.fun = 1;
    srv.request.pin = 1;
    srv.request.state = 1;
    ros::service::waitForService("/ur_driver/set_io");
    if (client.call(srv))
    {
      if(srv.response.success)
      {
        ROS_INFO_STREAM("IO succesfully toggled");
      }
      else
      {
        ROS_ERROR("Something went wrong with toggling the IO");
      }
    }
    else
    {
      ROS_ERROR("Failed to call service");
    }
  }

  void SuctionOff()
  {
    ur_msgs::SetIO srv;
    //Turn on suction
    srv.request.fun = 1;
    srv.request.pin = 1;
    srv.request.state = 0;
    ros::service::waitForService("/ur_driver/set_io");
    if (client.call(srv))
    {
      if(srv.response.success)
      {
        ROS_INFO_STREAM("IO succesfully toggled");
      }
      else
      {
        ROS_ERROR("Something went wrong with toggling the IO");
      }
    }
    else
    {
      ROS_ERROR("Failed to call service");
    }
  
  }
  void GraspObjects(const grasp_execution::GraspPose& msg)
  {
    //---------------------------------------------------- Add path constraints (Currently doesnt result in a planned path) ---------------------------------------------

    // moveit_msgs::OrientationConstraint orientation_constraint;
    // orientation_constraint.link_name = "ee_link";
    // orientation_constraint.header.frame_id = "base_link";
    // orientation_constraint.orientation.w = 1.0;
    // orientation_constraint.absolute_x_axis_tolerance = 0.1;
    // orientation_constraint.absolute_y_axis_tolerance = 0.1;
    // orientation_constraint.absolute_z_axis_tolerance = 0.1;
    // orientation_constraint.weight = 1.0;

    // moveit_msgs::Constraints test_constraints;
    // test_constraints.orientation_constraints.push_back(orientation_constraint);
    // move_group_arm->setPathConstraints(test_constraints);

    //--------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    ROS_INFO_STREAM("Objects Detected!");

    //-----------------------------------_Add objects to moveit planning scene ------------------------------------
    for(int i = 0; i < msg.num_objects; i ++)
    {
      moveit_msgs::CollisionObject temp_grasp_object;
      temp_grasp_object.header.frame_id = camera_frame;
      temp_grasp_object.id = "object" + std::to_string(i);
      temp_grasp_object.primitives.push_back(msg.object_shapes[i]);
      temp_grasp_object.primitive_poses.push_back(msg.object_poses[i].pose);
      temp_grasp_object.operation = temp_grasp_object.ADD;
      grasp_objects.push_back(temp_grasp_object);
    }    
    
    planning_scene_interface.addCollisionObjects(grasp_objects);
    planning_scene_interface.applyCollisionObjects(grasp_objects);
    ros::Duration(0.5).sleep();
    ROS_INFO_STREAM("Objects added to scene!");

    //-------------------------------------  Get Grasp poses from message --------------------------------------------------------------//
    
    geometry_msgs::PoseStamped grasp_pose;
    geometry_msgs::PoseStamped approach_pose;
    // geometry_msgs::TransformStamped transformation;
    // geometry_msgs::PoseStamped transformed_pose;

    float curr_x = 0;
    float curr_y = 0.3;

    for(int j = 0 ; j < msg.grasp_poses.size(); j ++)
    {
      //Create a PoseStamped version of a grasp_pose with parent frame as the camera
      grasp_pose= msg.grasp_poses[j];
      grasp_pose.pose.orientation = move_group_arm->getCurrentPose().pose.orientation;
      approach_pose = grasp_pose;
      approach_pose.pose.position.z += 0.2;
      if(MoveTo(approach_pose, move_group_arm) ==0){return ;} //Move to approach, 10cm above the object
      ros::Duration(2).sleep();
      PrintPose(grasp_pose.pose);

      geometry_msgs::PoseStamped down_pose = move_group_arm->getCurrentPose();
      down_pose.pose.position.z -= 0.2;
      PrintPose(down_pose.pose);
      ROS_WARN_STREAM("Move Down");
      if(MoveTo(down_pose, move_group_arm) ==0)
      {
        ROS_ERROR_STREAM("[ERROR] Move Down");
        return ;
      } //Move downwards to pick the object
      //if(MoveTo(grasp_pose, move_group_arm) ==0){return ;} //Move downwards to pick the object
      if(!sim){SuctionOn();} // Grasp object
      ros::Duration(3).sleep();
      //TwoFingerGrasp();

      // ==================Uncomment later ============================
      // ROS_WARN_STREAM("Attach Object to End Effector");
      // std::vector<std::string> temp_remove{grasp_objects[j].id};
      // move_group_hand->attachObject(grasp_objects[j].id);
      // planning_scene_interface.removeCollisionObjects(temp_remove);
      // ros::Duration(2).sleep();
      //===============================================================

      geometry_msgs::PoseStamped up_pose = move_group_arm->getCurrentPose();
      up_pose.pose.position.z += 0.2;

      //if(MoveTo(approach_pose, move_group_arm) ==0)
      if(MoveTo(up_pose, move_group_arm) ==0)
      {
        ROS_ERROR_STREAM("[ERROR] Move Up");
        return ;
      }
      ROS_WARN_STREAM("Moved Up");
      //if(MoveToJointValues(home_pose, move_group_arm) == 0){return;}
      //robot_state = planning_scene_monitor::LockedPlanningSceneRO(psm)->getCurrentStateUpdated(response.trajectory_start);
      float release_height = grasp_pose.pose.position.z;
      ros::Duration(1).sleep();

      // geometry_msgs::PoseStamped dropoff_pose;
      // dropoff_pose.header.frame_id = robot_frame;
      // dropoff_pose.pose = move_group_arm->getCurrentPose().pose;
      // dropoff_pose.pose.position.x = curr_x;
      // dropoff_pose.pose.position.y = curr_y;// Currently hard coded, need to change
      // //dropoff_pose.pose.position.z = release_height  + 0.2; // Currently hardcoded, need to change
      // //if(MoveTo(dropoff_pose, move_group_arm, planning_pipeline,psm, &response, robot_group) ==0){return;}
      // if(MoveTo(dropoff_pose, move_group_arm) ==0){return;}
      // dropoff_pose.pose.position.z = release_height; // Currently hardcoded, need to change
      // if(MoveTo(dropoff_pose, move_group_arm) ==0)
      // {
      //   ROS_ERROR_STREAM("[ERROR] Moved to Dropoff");
      //   return;
      // }

      if(MoveToJointValues(home_pose, move_group_arm) == 0){return;} // Move to home position
      ROS_WARN_STREAM("Moved to Dropoff");

      geometry_msgs::PoseStamped dropoff_pose;
      dropoff_pose.header.frame_id = robot_frame;
      dropoff_pose.pose = move_group_arm->getCurrentPose().pose;
      dropoff_pose.pose.position.z = 0.05 + 0.2285 ; // Currently hardcoded, need to change
      if(MoveTo(dropoff_pose, move_group_arm) ==0)
      {
        ROS_ERROR_STREAM("[ERROR] Moved to Dropoff");
        return;
      }

      if(!sim){SuctionOff();}
      ros::Duration(2).sleep();
      ROS_INFO_STREAM("Detach Object from End Effector");
      

      // ==================Uncomment later ============================
      // move_group_hand->detachObject(grasp_objects[j].id);
      // planning_scene_interface.applyCollisionObjects(grasp_objects[j]);
      // ros::Duration(2).sleep();
      //===============================================================
      
      if(curr_x >  0.5)
      {
        curr_x = 0;
      }
      else
      {
        curr_x -= 0.1;
      }
      
      //TwoFingerRelease();

      object_ids.push_back(grasp_objects[j].id);
      if(MoveToJointValues(home_pose, move_group_arm) == 0){return;} // Move to home position      
      ros::Duration(2).sleep();
    }
    planning_scene_interface.removeCollisionObjects(object_ids);
    object_ids.clear();
    ROS_INFO_STREAM("Done");
  }
};

#endif /* ENVIRONMENT_ */