
<!-- # Unsupervised Learning, depth based approach to generating 6 DOF grasp poses for novel objects -->

### This repository provides a basic tutorial on how to generate a robotic workcell with your collaborative robot, end effectors as well as other environmental objects . Currently preloaded scenes include  UR5 with a Robotiq-2F gripper and the UR5 with a single suction cup

|Robotiq 2F Gripper                     | Suction Gripper  |
| ------------- | -----|
| ![Robotiq-2F scene](./images/robotiq2f.png)  | ![Suction scene](./images/suction.png)|


To run the examples in a simulated environment, launch the following package 

#### For suction

``` roslaunch suction suction_env.launch``` 

#### For 2 Finger gripper

``` roslaunch robotiq_2f robotiq_2f_env.launch``` 

# Folder structure

In order for the workcell simulation to work well, we need to have an organized folder environment. Future documentation will include the standardization required for object folders, but for now the following shows the main 2 folders and subfolders needed for the environment. 

## __workcell_description__

This folder contains the description folders and moveit folders for the workcell. Includes custom and premade description packages. This folder contains the followng sub-folders: 

### robots

Contains the robot description and the moveit packages. Each robot should have their own folder `robot` with the sub folders `robot_description` and `robot_moveit_config`

### end_effectors

Contains the end effector description and the moveit packages. Each end effector should have their own folder `endeffector` with the sub folders  `endeffector_description` and `endeffector_moveit_config`

### environment

Contains the description packages for all other objects in the workcell that are not actuated. These objects are still important to be collision objects in the scene during path planning

## __workcell__

workcell provides the combined workcell urdf and the required launch files to launch the robot simulation for path planning.

# Describing your workcell

For easy accessing, we will describe each scene  as a separate folder, in the folder workcell/scenes/`your_scene` . You can copy current available scenes to make your own (making sure you edit the required parts)

## Visualize the workcell

To connect all elements in your workcell and visualize them in RViz, an environment urdf needs to be created, named `environment.urdf.xacro`. Here you will include all the relevant URDFs from the `workcell_description` folders

## Combining a Robot and End Effector 

To combine a robot with an end effector, a separate srdf file needs to be created to describe the two components and to avoid collisions. For generalization sake, this file will be named `ur5_arm_hand.srdf.xacro`. In each of these files, it is recommended to ignore collisions with the relevant links between the robot and the end effector. One of such collision is between the __tip link of the robot and the base link of the end effector.__

## Edited launch files

There are 2 launch files generated by the robot's moveit package that are required to be edited to visualize your custom environment in the simulation. 

### 1) move_group.launch

line 5 : `<include file="$(find workcell)/scenes/`<your_scene>`/launch/planning_context.launch">`

### 2) planning_context.launch
line 11 : `   <param name="$(arg robot_description)" command="$(find xacro)/xacro '$(find workcell)/scenes/`<your_scene>`/urdf/environment.urdf.xacro'" />`

if you have an end effector attached, 

line 15: `  <param name="$(arg robot_description)_semantic" command="$(find xacro)/xacro '$(find workcell)/scenes/`<your_scene>`/urdf/ur5_arm_hand.srdf.xacro'"/>`

if there is no end effector, 

line 15: `  <param name="$(arg robot_description)_semantic" command="$(find xacro)/xacro '$(find workcell)/scenes/`<your_scene>`/urdf/ur5.srdf.xacro'"/>`

## Main Launch file


# Moving the Robot in simulations using Moveit. 

## Changing Initial Pose of Robot

## Changing Default Path Planner of Robot

## Changing Joint Limits of Robot
